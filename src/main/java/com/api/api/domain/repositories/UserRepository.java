package com.api.api.domain.repositories;

import org.springframework.stereotype.Repository;

import java.util.Optional;

import com.api.api.domain.model.User;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
    Optional<User> findByEmail(String email);
}
