package com.api.api.config;

import java.util.List;

import com.api.api.domain.model.User;
import com.api.api.domain.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;



@Configuration
@Profile("local")
public class LocalConfig {
    
    @Autowired
    private UserRepository repository;    

    @Bean
    public void startDB () {
        User u1 = new User(null, "Lipe", "lipe@hotmail.com", "123");
        User u2 = new User(null, "FIlipe", "filipe@gamail.com,", "123");
        repository.saveAll(List.of(u1, u2));

    }


}
