package com.api.api.services;

import java.util.List;

import com.api.api.domain.model.User;
import com.api.api.dto.UserDTO;

public interface UserService {
    User findById (Integer id);
    List<User> findAll();
    User create(UserDTO obj);
    User update(UserDTO obj);
    void delete(Integer id);
}
