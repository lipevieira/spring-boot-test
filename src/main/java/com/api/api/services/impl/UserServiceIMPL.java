package com.api.api.services.impl;

import java.util.List;
import java.util.Optional;

import com.api.api.domain.model.User;
import com.api.api.domain.repositories.UserRepository;
import com.api.api.dto.UserDTO;
import com.api.api.services.UserService;
import com.api.api.services.exceptions.DataIntegratyViolationExcepion;
import com.api.api.services.exceptions.ObjectNotFoundException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceIMPL implements UserService{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ModelMapper mapper;

    @Override
    public User findById(Integer id) {
        Optional<User> obj = userRepository.findById(id);    
        return obj.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado"));   
    }

    public List<User> findAll () {
        return userRepository.findAll();
    }

    @Override
    public User create(UserDTO obj) {
        findByEmail(obj);
        return userRepository.save(mapper.map(obj, User.class));
    }

    private void findByEmail(UserDTO userDTO){
        Optional<User> user = userRepository.findByEmail(userDTO.getEmail());
        if(user.isPresent() && !user.get().getId().equals(userDTO.getId())) {
            throw new DataIntegratyViolationExcepion("E-mail já cadastrado no sistema.");
        }
    }

    @Override
    public User update(UserDTO obj) {
        findByEmail(obj);
        return userRepository.save(mapper.map(obj, User.class));
    }

    @Override
    public void delete(Integer id) {
        findById(id);
        userRepository.deleteById(id);
    }
    
}
