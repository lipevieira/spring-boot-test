package com.api.api.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import com.api.api.domain.model.User;
import com.api.api.domain.repositories.UserRepository;
import com.api.api.dto.UserDTO;
import com.api.api.services.exceptions.DataIntegratyViolationExcepion;
import com.api.api.services.exceptions.ObjectNotFoundException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.yaml.snakeyaml.events.Event.ID;

public class UserServiceIMPLTest {

    /**
     * Index da referente ao primeiro item da lista
     */
    private static final int INDEX = 0;

    /**
     * Messagem padrão para objeto não encontrado
     */
    private static final String OBJETO_NAO_ENCONTRADO = "Objeto não encontrado";

    private static final String PASSWORD = "123";

    private static final String EMAIL = "teste01@gmail.com";

    private static final String NAME = "Teste 01";

    private static final Integer ID = 1;

    @InjectMocks
    private UserServiceIMPL userServiceIMPL;

    @Mock
    private UserRepository userRepository;

    @Mock
    private ModelMapper mapper;

    private User user;
    private UserDTO userDTO;
    private Optional<User> userOptinal;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        startUser();
    }

    @Test
    void whenCreateThenReturnSuccess() {
        // Mocando a chamda do metodo
        when(userRepository.save(any())).thenReturn(user);

        User response = userServiceIMPL.create(userDTO);

        assertNotNull(response);
        assertEquals(User.class, response.getClass());

        assertEquals(ID, response.getId());
        assertEquals(NAME, response.getName());
        assertEquals(EMAIL, response.getEmail());
        assertEquals(PASSWORD, response.getPassword());
    }

    @Test
    void whenUpdateThenReturnSuccess() {
        // Mocando a chamda do metodo
        when(userRepository.save(any())).thenReturn(user);

        User response = userServiceIMPL.update(userDTO);

        assertNotNull(response);
        assertEquals(User.class, response.getClass());

        assertEquals(ID, response.getId());
        assertEquals(NAME, response.getName());
        assertEquals(EMAIL, response.getEmail());
        assertEquals(PASSWORD, response.getPassword());
    }

    @Test
    void whenCreateThenReturnAnDataIntergrityViolationException() {
        // Mocando a chamda do metodo
        when(userRepository.findByEmail(anyString())).thenReturn(userOptinal);

        try {
            userOptinal.get().setId(2);
            userServiceIMPL.create(userDTO);
        } catch (Exception ex) {
            assertEquals(DataIntegratyViolationExcepion.class, ex.getClass());
            assertEquals("E-mail já cadastrado no sistema.", ex.getMessage());
        }
    }

    @Test
    void whenUpdateThenReturnAnDataIntergrityViolationException() {
        // Mocando a chamda do metodo
        when(userRepository.findByEmail(anyString())).thenReturn(userOptinal);

        try {
            userOptinal.get().setId(2);
            userServiceIMPL.create(userDTO);
        } catch (Exception ex) {
            assertEquals(DataIntegratyViolationExcepion.class, ex.getClass());
            assertEquals("E-mail já cadastrado no sistema.", ex.getMessage());
        }
    }

    @Test
    void testDeleteWithSuccess() {
        when(userRepository.findById(anyInt())).thenReturn(userOptinal);
        // Quando chamar o metodo deleteById não faz nada o pq o metodo é void
        doNothing().when(userRepository).deleteById(anyInt());
        userServiceIMPL.delete(ID);
        // veriifcar a quantidade de vezes que o metodo deleteById foi chamado.
        verify(userRepository, times(1)).deleteById(anyInt());
    }

    void deleteWithObjectNotFoundExeption() {
        when(userRepository.findById(anyInt()))
                .thenThrow(new ObjectNotFoundException("Objeto não encontrado."));
        try {
            userServiceIMPL.delete(ID);
        } catch (Exception e) {
            assertEquals(ObjectNotFoundException.class, e.getClass());
            assertEquals("Objeto não encontrado.", e.getMessage());
        }
    }

    /**
     * Quando busca todos então retorne uma lista de usuários
     */
    @Test
    void whenFindAllThenReturnAnListOfUsers() {
        // Mocando a chamda do metodo.
        when(userRepository.findAll()).thenReturn(List.of(user));

        List<User> response = userServiceIMPL.findAll();

        assertNotNull(response);
        assertEquals(1, response.size());
        assertEquals(User.class, response.get(INDEX).getClass());

        assertEquals(ID, response.get(INDEX).getId());
        assertEquals(NAME, response.get(INDEX).getName());
        assertEquals(EMAIL, response.get(INDEX).getEmail());
        assertEquals(PASSWORD, response.get(INDEX).getPassword());
    }

    @Test
    void whenFindByIdThenReturnAnUserInstance() {
        when(userRepository.findById(anyInt())).thenReturn(userOptinal);
        User response = userServiceIMPL.findById(ID);

        assertNotNull(response);
        assertEquals(User.class, response.getClass());
        assertEquals(ID, response.getId());
        assertEquals(NAME, response.getName());
        assertEquals(EMAIL, response.getEmail());

    }

    @Test
    void whenFindBiIdReturnAnObjectNotFoundException() {
        when(userRepository.findById(anyInt())).thenThrow(new ObjectNotFoundException(OBJETO_NAO_ENCONTRADO));
        try {
            userServiceIMPL.findById(ID);
        } catch (Exception ex) {
            assertEquals(ObjectNotFoundException.class, ex.getClass());
            assertEquals(OBJETO_NAO_ENCONTRADO, ex.getMessage());
        }
    }

    @Test
    void testUpdate() {

    }

    /**
     * Inicializar objetos de User
     */
    private void startUser() {
        user = new User(ID, NAME, EMAIL, PASSWORD);
        userDTO = new UserDTO(1, NAME, EMAIL, PASSWORD);
        userOptinal = Optional.of(new User(ID, NAME, EMAIL, PASSWORD));
    }
}
